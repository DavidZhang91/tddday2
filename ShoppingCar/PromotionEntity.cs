﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingCar
{
    public class PromotionEntity
    {
        public decimal Discount { get; set; }
        public decimal Price { get; set; }
        public int Qty { get; set; }
        public decimal Rate { get; set; }
        public string Type { get; set; }
    }
}
