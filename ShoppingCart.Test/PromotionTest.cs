﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xunit;
using FluentAssertions;
using NSubstitute;
using ShoppingCar;

namespace UnitTestHomework.Test
{
    /// <summary>
    /// ShoppingCartTest 的摘要描述
    /// </summary>
    public class PromotionTest
    {
        public PromotionTest()
        {

        }

        [Fact]
        public void Test_滿額500_折100()
        {
            PromotionEntity entity = new PromotionEntity
            {
                Type = "TotalPrice",
                Price = 500m,
                Discount = 100m
            };

            //// Act
            Promotion promotion = new Promotion();
            promotion.Insert(entity);

            //// Assert
            promotion.Received().Insert(entity);
        }

        [Fact]
        public void Test_滿3件_打9折()
        {
            //// Arrange
            PromotionEntity entity = new PromotionEntity
            {
                Type = "TotalQty",
                Qty = 3,
                Rate = 0.9m
            };

            //// Act
            Promotion promotion = new Promotion();
            promotion.Insert(entity);

            //// Assert
            promotion.Received().Insert(entity);
        }
    }
}
